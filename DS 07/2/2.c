/*
2014105075 이지원
본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
*/

#include <stdio.h>
#include <stdlib.h>

typedef struct listNode
{
	int data;
	struct listNode* link;
} listNode;

listNode* find();
void create();
void insert(listNode** first, listNode* x);
void delete(listNode** first, listNode* trail, listNode* x);
void deleteNode_50();
void deleteNode_All();
void printList(listNode* first);

FILE *f;
listNode* first = NULL;

int main(void)
{
	listNode* x, temp;

	f = fopen("input.txt", "r");
	x = (listNode*)malloc(sizeof(*x));

	create();

	while (!feof(f))
		insert(&first, x);

	printf("The ordered list contain : \n");
	printList(first);

	printf("\nAfter deleting nodes with data less than and equal to 50\n");
	printf("The ordered list contain : \n");
	deleteNode_50();
	printList(first);

	deleteNode_All();
	//printList(first);

	fclose(f);

	return 0;
}

listNode* find(int num)
{
	listNode* temp = first;

	for (; temp->link != NULL; temp = temp->link)
		if (temp->link->data >= num)
			break;

	return temp;
}

void create()
{
	listNode *a, *b;
	int temp;

	a = (listNode*)malloc(sizeof(*a));
	b = (listNode*)malloc(sizeof(*b));

	fscanf(f, "%d %d", &a->data, &b->data);

	if (a->data > b->data)
	{
		b->link = a;
		a->link = NULL;
		first = b;
	}
	else
	{
		a->link = b;
		b->link = NULL;
		first = a;
	}
}

void insert(listNode* *first, listNode* x)
{
	listNode* temp, linktemp;
	int num;

	temp = (listNode*)malloc(sizeof(*temp));
	fscanf(f, "%d", &num);

	x = find(num);

	temp->data = num;

	if (x == *first) // 첫 칸에 추가
	{
		temp->link = *first;
		*first = temp;
	}
	else if (x->link == NULL) // 끝 칸에 추가
	{
		x->link = temp;
		temp->link = NULL;
	}
	else // 중간에 추가
	{
		temp->data = num;
		temp->link = x->link;
		x->link = temp;
	}
}

void delete(listNode* *first, listNode* trail, listNode* x)
{
	if (trail)
		trail->link = x->link;
	else
		*first = (*first)->link;

	free(x);
}

void deleteNode_50()
{
	listNode* temp = first;

	for (; first->data <= 50; temp = first) // 정렬되있으므로 50 이상 나오기 전까지 지워줌
		delete(&first, NULL, temp);
}

void deleteNode_All()
{
	listNode* temp = first;

	for (; first; temp = first)
		delete(&first, NULL, temp);
}

void printList(listNode* first)
{
	int i;

	for (i = 1; first; first = first->link)
		printf("%4d%s", first->data, (i++ % 10) == 0 ? "\n" : (!(first->link) ? "\n" : ""));
}