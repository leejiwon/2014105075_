#include <stdio.h>
#include <stdlib.h>

typedef struct polyNode *polyPointer;
typedef struct polyNode {
	int coef;
	int expon;
	polyPointer link;
}polyNode;
polyPointer a, b;

int COMPARE(int a, int b)
{
	if (a < b)
		return -1;
	else if (a == b)
		return 0;
	else
		return 1;
}

polyPointer findLast(polyPointer first)
{
	for (; first; first = first->link) {
		if (first->link == NULL)
			return first;
	}
}

void insert(polyPointer *first, polyPointer x,int coef,int expon)
{
	polyPointer temp;
	temp = (polyPointer*)malloc(sizeof(*temp));
	temp->coef = coef;
	temp->expon = expon;
	if (*first) {
		temp->link = x->link;
		x->link = temp;
	}
	else {
		temp->link = NULL;
		*first = temp;
	}
}

void printList(polyPointer first)
{
	for (; first; first = first->link)
		printf("%+d*^%d  ", first->coef,first->expon);
	printf("\n");
}

void attach(int coef, int expon, polyPointer *ptr)
{
	polyPointer temp;
	temp = (polyPointer*)malloc(sizeof(*temp));
	temp->coef = coef;
	temp->expon = expon;
	(*ptr)->link = temp;
	*ptr = temp;
}

void erase(polyPointer *ptr)
{
	polyPointer temp;
	while (*ptr) {
		temp = *ptr;
		*ptr = (*ptr)->link;
		free(temp);
	}
}

polyPointer padd(polyPointer a, polyPointer b)
{
	polyPointer c, rear, temp;
	int sum;
	rear = (polyPointer*)malloc(sizeof(*rear));
	c = rear;
	while (a&&b)
		switch (COMPARE(a->expon, b->expon)) {
		case -1: //a->expon < b->expon
			attach(b->coef, b->expon, &rear);
			b = b->link;
			break;
		case 0: //a->expon = b->expon
			sum = a->coef + b->coef;
			if (sum) attach(sum, a->expon, &rear);
			a = a->link; b = b->link;
			break;
		case 1: //a->expon > b->expon
			attach(a->coef, a->expon, &rear);
			a = a->link;
	}

	for (; a; a = a->link) attach(a->coef, a->expon, &rear);
	for (; b; b = b->link) attach(b->coef, b->expon, &rear);
	rear->link = NULL;

	temp = c;
	c = c->link;
	free(temp);
	return c;
}

polyPointer createpoly(int firstcoef,int firstexpon,int secondcoef,int secondexpon)
{
	polyPointer first, second;
	first = (polyPointer*)malloc(sizeof(*first));
	second = (polyPointer*)malloc(sizeof(*second));
	second->link = NULL;
	second->coef = secondcoef;
	second->expon = secondexpon;
	first->coef = firstcoef;
	first->expon = firstexpon;
	first->link = second;
	return first;
}

void main()
{
	FILE* fa = fopen("a.txt", "r");
	FILE* fb = fopen("b.txt", "r");

	int coef1, expon1, coef2, expon2;
	polyPointer *c;
	fscanf(fa, "%d %d", &coef1, &expon1);
	fscanf(fa, "%d %d", &coef2, &expon2);
	a = createpoly(coef1, expon1, coef2, expon2);
	printf("    a :   ");
	while (fscanf(fa, "%d %d", &coef1, &expon1) != EOF) {
		insert(a, findLast(a), coef1, expon1);
	}
	printList(a);

	fscanf(fb, "%d %d", &coef1, &expon1);
	fscanf(fb, "%d %d", &coef2, &expon2);
	b = createpoly(coef1, expon1, coef2, expon2);
	printf("    b :   ");
	while (fscanf(fb, "%d %d", &coef1, &expon1) != EOF) {
		insert(b, findLast(b), coef1, expon1);
	}
	printList(b);

	c = padd(a, b);
	printf("a+b=c :   ");
	printList(c);

	erase(&a);
	erase(&b);
	erase(&c);
	fclose(fa);
	fclose(fb);
}