#include <stdio.h>
#define MAX_SIZE 20
#define SWAP(X,Y,Z) Z = X, X = Y, Y = Z

typedef struct element{
	int key;
}element;

element a[MAX_SIZE];

void adjust(element a[], int root, int n)
{
	int child,rootkey;
	element temp;
	temp = a[root];
	rootkey = a[root].key;
	child = 2*root;
	while (child <= n){
		if((child < n) && (a[child].key < a[child+1].key))
			child++;
		if(rootkey > a[child].key)
			break;
		else{
			a[child / 2] = a[child];
			child *= 2;
		}
	}
	a[child/2] = temp;
}

void printMerge(element a[],int n)
{
	int i;

	for(i=1;i<=n;i++)
		printf("%3d",a[i].key);
}

void heapSort(element a[], int n)
{
	int i;
	element temp;

	

	for(i = n/2; i > 0; i--){
		printf("a = ");
		adjust(a,i,n);
		printMerge(a,n);
		printf("\n");
	}
	for(i = n-1; i > 0; i--){
		printf("a = ");
		SWAP(a[1],a[i+1],temp);
		adjust(a,1,i);
		printMerge(a,n);
		printf("\n");
	}
}

void main()
{
	FILE *fi = fopen("input.txt","r"),*fo = fopen("output.txt","w");
	int i = 1;
	int n = 1;
	
	while(fscanf(fi,"%d",&a[n].key) != EOF)
	{
		n++;
	}
	n--;

	heapSort(a,n);

	while(i<=n)
	{
		fprintf(fo,"%d ",a[i].key);
		i++;
	}

	printf("\nfinal a = ");
	printMerge(a,n);

	printf("\n");

	fclose(fi);
	fclose(fo);
}