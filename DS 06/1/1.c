// 2014105075 이지원
// 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#include <stdio.h>
#include <stdlib.h>
#define MAX_STACK_SIZE 100
#define MAX_ROW 10
#define MAX_COL 10
#define TRUE 1
#define FALSE 0

typedef struct {
	short int vert;
	short int horiz;
} offsets;

offsets move[8] = { { -1, 0 }, { -1, 1 }, { 0, 1 }, { 1, 1 }, { 1, 0 }, { 1, -1 }, { 0, -1 }, { -1, -1 } };

typedef struct {
	short int row;
	short int col;
	short int dir;
} element;

element stack[MAX_STACK_SIZE];
int maze[MAX_ROW][MAX_COL] = { 0 };
int mark[MAX_ROW][MAX_COL] = { 0 };
int top = -1;
int EXIT_ROW, EXIT_COL;

void stackfull()
{
	fprintf(stderr, "Stack is full, cannot add element\n");
	exit(EXIT_FAILURE);
}

element stackEmpty()
{
	fprintf(stderr, "Stack is empty, cannot delete elements!\n");
	exit(EXIT_FAILURE);
}

void push(element item)
{
	if (top >= MAX_STACK_SIZE - 1) {
		stackfull();
	}
	stack[++top] = item;
}

element pop()
{
	if (top == -1) {
		return stackEmpty();
	}
	return stack[top--];
}

void path()
{
	int i, row, col, nextRow, nextCol, dir, found = FALSE;
	element position;
	mark[1][1] = 1;
	top = 0;
	stack[0].row = 1;
	stack[0].col = 1;
	stack[0].dir = 1;

	while (top > -1 && !found) {
		position = pop();
		row = position.row;
		col = position.col;
		dir = position.dir;

		while (dir < 8 && !found) {
			nextRow = row + move[dir].vert;
			nextCol = col + move[dir].horiz;
			if (nextRow == EXIT_ROW&&nextCol == EXIT_COL) {
				found = TRUE;
			}
			else if (!maze[nextRow][nextCol] && !mark[nextRow][nextCol]) {
				mark[nextRow][nextCol] = 1;
				position.row = row;
				position.col = col;
				position.dir = ++dir;
				push(position);
				row = nextRow;
				col = nextCol;
				dir = 0;
			}
			else {
				++dir;
			}
		}
	}
	if (found) {
		printf("The path is  : \n");
		printf("row col\n");
		for (i = 0; i <= top; i++) {
			printf("%2d%5d\n", stack[i].row, stack[i].col);
		}
		printf("%2d%5d\n", row, col);
		printf("%2d%5d\n", EXIT_ROW, EXIT_COL);
	}
	else {
		printf("The maze does not have a path.\n");
	}
}

void main()
{
	int i, j;
	FILE *f = fopen("input3.txt", "r");
	fscanf(f, "%d %d", &EXIT_ROW, &EXIT_COL);
	for (i = 0; i < EXIT_ROW + 2; i++) {
		maze[i][0] = 1;
		maze[i][EXIT_COL + 1] = 1;
		mark[i][0] = 1;
		mark[i][EXIT_COL + 1] = 1;
	}
	for (i = 0; i < EXIT_COL + 2; i++) {
		maze[EXIT_ROW + 1][i] = 1;
		maze[0][i] = 1;
		mark[EXIT_ROW + 1][i] = 1;
		mark[0][i] = 1;
	}
	for (i = 1; i <= EXIT_ROW; i++) {
		for (j = 1; j <= EXIT_COL; j++) {
			fscanf(f, "%d", &maze[i][j]);
		}
	}
	for (i = 0; i < EXIT_ROW + 2; i++) {
		for (j = 0; j < EXIT_COL + 2; j++) {
			printf("%d ", maze[i][j]);
		}
		printf("\n");
	}
	printf("\n");
	path();
	fclose(f);
}