#include <stdio.h>
#include <stdlib.h>

#define COMPARE(x, y) ( ((x) < (y)) ? -1 : ((x) == (y)) ? 0: 1 )
#define FALSE 0
#define TRUE 1

typedef struct listNode *listPointer;
typedef struct listNode
{
	int coef;
	int expon;
	listPointer link;
} listNode;

listPointer a, b, lastA, lastB, avail;

void insertLast(listPointer *last, listPointer node);
void printCList(listPointer x);
void attach (float coefficient, int exponent, listPointer *ptr);
void erase (listPointer *ptr);
listPointer cpadd(listPointer a, listPointer b);
listPointer getNode();
void retNode(listPointer node);
void cerase(listPointer *ptr);

int main(void)
{
	listPointer result;
	int c, e;
	FILE *f1 = fopen("a.txt", "r");
	FILE *f2 = fopen("b.txt", "r");
	
	a = (listPointer) malloc(sizeof(*a));
	b = (listPointer) malloc(sizeof(*b));
	a->expon = -1;
	b->expon = -1;
	lastA = a;
	lastB = b;

	while(!feof(f1))
	{
		fscanf(f1, "%d %d", &c, &e);
		attach(c, e, &lastA);
	}
	lastA->link = a;

	while(!feof(f2))
	{	
		fscanf(f2, "%d %d", &c, &e);
		attach(c, e, &lastB);
	}
	lastB->link = b;

	result = cpadd(a, b);

	printf("%6c : ", 'a');
	printCList(a);
	printf("%6c : ", 'b');
	printCList(b);
	printf("%6s : ", "a+b=c");
	printCList(result);

	cerase(&a);
	cerase(&b);
	cerase(&result);
	erase(&avail);

	fclose(f1);
	fclose(f2);

	return 0;
}

void insertLast(listPointer *last, listPointer node)
{
	if (!(*last))
	{
		*last = node;
		node->link = node;
	}
	else
	{
		node->link = (*last)->link;
		(*last)->link = node;
		*last = node;
	}
}

void printCList(listPointer x)
{
	x = x->link;

	while(x->expon != -1)
	{
		printf("%+4dx^%2d", x->coef, x->expon);
		x = x->link;
	}

	printf("\n");
}

void attach (float coefficient, int exponent, listPointer *ptr)
{
	listPointer temp;
	
	temp = getNode();
	temp->coef = coefficient;
	temp->expon = exponent;
	(*ptr)->link = temp;
	*ptr = temp;
}


void erase (listPointer *ptr)
{
	listPointer temp;

	while (*ptr)
	{
		temp = *ptr;
		*ptr = (*ptr)->link;
		free(temp);
	}
}

listPointer cpadd(listPointer a, listPointer b)
{
	listPointer startA, c, lastC;
	int sum, done = FALSE;

	startA = a;
	a = a->link;
	b = b->link;
	c = getNode();
	c->expon = -1;
	lastC = c;

	do
	{
		switch (COMPARE(a->expon, b->expon))
		{
			case -1:
				attach(b->coef, b->expon, &lastC);
				b = b->link;
				break;
			case 0:
				if (startA == a)
					done = TRUE;
				else
				{
					sum = a->coef + b->coef;
					if (sum)
						attach(sum, a->expon, &lastC);
					a = a->link;
					b = b->link;
				}
				break;
			case 1:
				attach(a->coef, a->expon, &lastC);
				a = a->link;
		}
	} while (!done);

	lastC->link = c;

	return c;
}

listPointer getNode()
{
	listPointer node;
	if (avail)
	{
		node = avail;
		avail = avail->link;
	}
	else
		node = (listPointer) malloc(sizeof(*node));
}

void retNode(listPointer node)
{
	node->link = avail;
	avail = node;
}

void cerase(listPointer *ptr)
{
	listPointer temp;
	if (*ptr)
	{
		temp = (*ptr)->link;
		(*ptr)->link = avail;
		avail = temp;
		*ptr = NULL;
	}
}