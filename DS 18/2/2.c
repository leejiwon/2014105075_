#include <stdio.h>
#include <string.h>
#define MAX_VALUE 20

typedef struct {
	int key;
	char name[10];
	int grade;
}element;
element str[MAX_VALUE];

void insert(element e, element a[], int i)
{
	a[0] = e;
	while (e.key < a[i].key)
	{
		a[i + 1] = a[i];
		i--;
	}
	a[i + 1] = e;
}

void insertionSort(element a[], int n)
{
	int j;
	for (j = 2; j <= n; j++)
	{
		element temp = a[j];
		insert(temp, a, j - 1);
	}
}

void main()
{
	FILE* fin = fopen("input.txt", "r");
	FILE* fout = fopen("output.txt", "w");
	int max_num, i=1, key, grade;
	char name[10];

	fscanf(fin, "%d", &max_num);

	while (fscanf(fin, "%d %s %d", &key, name, &grade) != EOF)
	{
		str[i].key = key;
		strcpy(str[i].name, name);
		str[i].grade = grade;
		
		i++;
	}

	insertionSort(str, max_num);

	for (i = 1; i < max_num+1; i++)
	{
		printf("%d %s %d\n", str[i].key, str[i].name, str[i].grade);
		fprintf(fout, "%d %s %d\n", str[i].key, str[i].name, str[i].grade);
	}

	fclose(fin);
	fclose(fout);
}