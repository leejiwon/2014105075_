#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MALLOC(p, s)\
    if(!((p) = malloc(p,s))){\
        fprintf(stderr, "insufficient memory");\
        exit(EXIT_FAILURE);\
	            }

typedef struct{
	int no;
	char name[20];
}element;

element *queue;
int capacity = 2;
int rear = 0;
int front = 0;

void addq(element item);
element deleteq();
void queueFull();
element queueEmpty();
void copy(element *a, element *b, element *c);

int main()
{
	int i = 0, j = 0;
	char ordering[20];
	char orderword[5];
	int queuenumber;
	char supplyname[20];

	element temp;

	queue = (element *)malloc(sizeof(element) * capacity);

	printf("<< circular queue operations where the initial capacity is 2>>\n***************************************************\n");

	for (i = 0;; i++)
	{
		gets(ordering);
		if (strcmp(ordering, "d") == 0)
		{

			if (front == rear)
			{
				printf("queue is empty, cannot delete element.\n");
				exit(EXIT_FAILURE);
			}

			deleteq();
			*(queue + front)->name = NULL;
			(queue + front)->no = NULL;

		}
		else
		{
			sscanf(ordering, "%s %d %s", &orderword, &queuenumber, &supplyname);
			temp.no = queuenumber;
			strcpy(temp.name, supplyname);

			addq(temp);

		}

	}


	return 0;
}

void addq(element item)
{
	rear = (rear + 1) % capacity;
	if (front == rear)
		queueFull();
	queue[rear] = item;
}

element deleteq()
{
	element item;
	if (front == rear)
		return queueEmpty();

	front = (front + 1) % capacity;
	printf("deleted item : %d %s\n", (queue + front)->no, (queue + front)->name);
	return queue[front];
}

void queueFull()
{
	int start;

	element *newQueue;

	newQueue = (element *)malloc(2 * capacity*sizeof(*queue));

	start = (front + 1) % capacity;

	rear--;

	if (start < 2)
		copy(queue + start, queue + start + capacity - 1, newQueue);
	else
	{
		copy(queue + start, queue + capacity, newQueue);
		copy(queue, queue + rear + 1, newQueue + capacity - start);
	}

	front = 2 * capacity - 1;
	rear = capacity - 1;
	capacity *= 2;
	free(queue);
	queue = newQueue;

	printf("queue capacity is doubled.\ncurrent queue capacity is %d\n", capacity);
}

void copy(element *a, element *b, element *c)
{
	int i;
	for (i = 0; a + i != b; i++)
	{
		(c + i)->no = (a + i)->no;
		strcpy((c + i)->name, (a + i)->name);
	}
}

element queueEmpty()
{
	element item;
	item.no = -1;
	return item;
}