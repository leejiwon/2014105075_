#include <stdio.h>
#define MAX_SIZE 20

typedef struct element{
	int key;
}element;

element a[MAX_SIZE];


void merge(element initList[], element mergedList[], int i, int m, int n)
{
	int j,k,t;
	j = m+1;
	k = i;

	while ( i <= m && j <= n){
		if (initList[i].key <= initList[j].key)
			mergedList[k++] = initList[i++];
		else
			mergedList[k++] = initList[j++];
	}
	if( i > m )
		for(t = j; t <= n; t++)
			mergedList[t] = initList[t];
	else
		for(t = i; t <= m; t++)
			mergedList[k+t-i] = initList[t];
}

void mergePass(element initList[], element mergedList[],int n, int s)
{
	int i,j;
	for(i = 1; i+2*s-1 <= n; i += 2*s)
		merge(initList,mergedList,i,i+s-1,i+2*s-1);
	if(i+s-1 < n)
		merge(initList,mergedList,i,i+s-1,n);
	else
		for(j = i; j <= n; j++)
			mergedList[j] = initList[j];
}

void mergeSort(element a[], int n)
{
	int s = 1;
	int k;
	element extra[MAX_SIZE];

	while (s < n){
		mergePass(a,extra,n,s);
		
		printf("s = %d\n",s);
		printf("a = ");
		for(k=1;k<=n;k++)
			printf("%3d",a[k].key);
		printf("\nextra = ");
		for(k=1;k<=n;k++)
			printf("%3d",extra[k].key);
		printf("\n\n");
		
		s*= 2;

		mergePass(extra,a,n,s);
		
		printf("s = %d\n",s);
		printf("a = ");
		for(k=1;k<=n;k++)
			printf("%3d",a[k].key);
		printf("\nextra = ");
		for(k=1;k<=n;k++)
			printf("%3d",extra[k].key);
		printf("\n\n");
		
		s *= 2;
	}
}

void printMerge(element a[],int n)
{
	int i;

	for(i=1;i<=n;i++)
		printf("%d ",a[i].key);
}

void main()
{
	FILE *fi = fopen("input.txt","r"),*fo = fopen("output.txt","w");

	int i=1;
	int n = 1;

	while(fscanf(fi,"%d",&a[n].key)!=EOF)
	{
		n++;
	}
	n--;
	mergeSort(a,n);

	while(i<=n)
	{
		fprintf(fo,"%d ",a[i].key);
		i++;
	}

	printf("final a = ");
	printMerge(a,n);
	printf("\n");

	fclose(fi);
	fclose(fo);
}