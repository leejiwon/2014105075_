#include <stdio.h>
#include <stdlib.h>
#define MAX_SIZE 20

typedef struct element{
	int key;
}element;

element a[MAX_SIZE];
int link[MAX_SIZE];
int d,keys;
FILE* fo;

int digit(element a,int i,int r)
{
	int makeDigit = a.key;
	int k,exponent=1;

	for(k = 0;k < d-i-1; k++)
		exponent *= 10;

	if(a.key<exponent)
		return 0;

	makeDigit /= exponent;
	makeDigit %= 10;

	return makeDigit;
}

void printlist(element a[],int d,int n,int first)
{
	int z, k;

	k = first;

	fo = fopen("output.txt","w");

	for(z=1;z<=keys;z++)
		printf(" [%2d]",z);
	printf("\n   link:");

	for(z=1;z<=keys;z++)
		printf("   %2d",link[z]);
	printf("\n      a:");

	for(z=1;z<=keys;z++)
		printf("   %2d",a[z].key);
	printf("\n  first:  %2d",first);

	printf("\n\n result:");

	printf("   %2d",a[k].key);
	fprintf(fo,"   %2d",a[k].key);
	for(z=1;z<keys;z++)
	{
		printf("   %2d",a[link[k]].key);
		fprintf(fo,"   %2d",a[link[k]].key);
		k = link[k];
	}
	printf("\n");

	fclose(fo);
}

int radixSort(element a[], int link[], int d,int r, int n)
{
	int *front,*rear;
	int i,j,bin,current,first,last;
	first = 1;
	for(i = 1;i < n;i++)
		link[i] = i+1;
	link[n] = 0;

	front = (int *)malloc(sizeof(int) * r);
	rear = (int *)malloc(sizeof(int) * r);


	printf("********************** initial chain **********************\n\t");
	printlist(a,d,keys,first);

	for(i = d-1; i >= 0;i--)
	{

		for(bin = 0;bin < r;bin++){
			front[bin] = 0;
			rear[bin] = 0;
		}
		for(current = first; current; current = link[current])
		{
			bin = digit(a[current],i,r);
			if (front[bin] == 0) front[bin] = current;
			else link[rear[bin]] = current;
			rear[bin] = current;
		}

		for(bin = 0; !front[bin]; bin++);
		first = front[bin];
		last = rear[bin];

		for(bin++; bin < r; bin++)
			if(front[bin])
			{link[last] = front[bin]; last = rear[bin];}
			link[last] = 0;

			printf("\n********************** pass %d **********************\n\t",d-i);
			printlist(a,d,keys,first);

			printf("\n\t");
			for(j=0;j<r;j++)
				printf(" [%2d ]",j);
			printf("\n  front:");
			for(j=0;j<r;j++)
				printf("  %3d ",front[j]);
			printf("\n   rear:");
			for(j=0;j<r;j++)
				printf("  %3d ",rear[j]);
	}

	printf("\n");

	return first;
}

void main()
{
	FILE *f = fopen("input.txt","r");
	int x=1;
	int first;
	fscanf(f,"%d %d",&d,&keys);

	while(fscanf(f,"%d",&a[x].key)!=EOF)
	{
		x++;
	}

	fclose(f);

	radixSort(a,link,d,10,keys);
}