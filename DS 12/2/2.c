#include <stdio.h>
#include <stdlib.h>
#define MAX_ELEMENTS 200
#define HEAP_FULL(n) (n == MAX_ELEMENTS-1)
#define HEAP_EMPTY(n) (!n)
typedef struct {
	int key;
}element;
element heap[MAX_ELEMENTS];
int n = 0;

void push(element item, int *n)
{//사이즈가 n이고 max heap가 들어간 item을 insert!
	int i;
	if (HEAP_FULL(*n)) {//Full
		fprintf(stderr, "The heap is full. \n");
		exit(EXIT_FAILURE);
	}
	i = ++(*n); //i는 현재 위치, 넣을려는 위치로 이동
	while ((i != 1) && (item.key < heap[i / 2].key)) {//갯수가 1개가 아니고 넣을려는 수가 원래 있던 수보다 크면 교체
		heap[i] = heap[i / 2];
		i /= 2; //넣을려는 위치 이동
	}
	heap[i] = item;
}

element pop(int *n)
{//큰수부터 pop
	int parent, child, j;
	element item, temp;
	if (HEAP_EMPTY(*n)) {//Empty
		fprintf(stderr, "The heap is empty\n");
		exit(EXIT_FAILURE);
	}

	item = heap[1];//가장 큰수를 item에 넣는다
	temp = heap[(*n)--];//마지막 수를 넣는다
	parent = 1;
	child = 2;
	while (child <= *n) {//현재 parant의 child중 가장 큰수를 찾는다
		if ((child < *n) && (heap[child].key > heap[child + 1].key))
			child++;
		if (temp.key <= heap[child].key)
			break;
		heap[parent] = heap[child];//밑으로 이동
		parent = child;
		child *= 2;
	}
	heap[parent] = temp;
	return item;
}

void main()
{
	FILE* fp = fopen("input.txt", "r");
	element tp;
	int num, i, j, count = 0;

	printf("***** insertion into a max heap *****\n");
	while (fscanf(fp, "%d", &num) != EOF) {
		tp.key = num;
		count++;
		push(tp, &n);
		for (i = 1; i <= count; i++) {
			printf("%3d", heap[i].key);
		}
		printf("\n");
	}

	printf("***** deletion from a max heap *****\n");
	count--;
	for (;;) {
		pop(&n);

		for (i = 1; i <= count; i++) {
			printf("%3d", heap[i].key);
		}
		printf("\n");
		count--;
	}
}