#include <stdio.h>
#define MAX_VALUE 20
#define SWAP(x,y,t) ((t)=(x),(x)=(y),(y)=(t))  
typedef struct {
	int key;
}element;
element str[MAX_VALUE];
int countt=0;

void quickSort(element a[], int left, int right)
{
	int pivot, i, j;
	element temp;
	countt++;
	if (left < right)
	{
		i = left;
		j = right + 1;
		pivot = a[left].key;
		do{
			do i++; while (a[i].key < pivot);
			do j--; while (a[j].key > pivot);
			if (i < j) SWAP(a[i], a[j], temp);
		} while (i < j);
		SWAP(a[left], a[j], temp);
		quickSort(a, left, j - 1);
		quickSort(a, j + 1, right);
	}
}

void main()
{
	FILE* fin = fopen("input.txt", "r");
	FILE* fout = fopen("output.txt", "w");
	int count = 0, i;

	while (fscanf(fin, "%d", &str[count].key) != EOF)
	{
		count++;
	}

	quickSort(str, 0, count - 1);
	
	for (i = 0; i < count; i++)
	{
		printf("%d\n", str[i].key);
		fprintf(fout, "%d\n", str[i].key);
	}

	printf("count = %d\n", countt);

	fclose(fin);
	fclose(fout);
}