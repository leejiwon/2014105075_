#include <stdio.h>
#include <stdlib.h>
#define MAX_SIZE 24
#define FALSE 0
#define TRUE 1

typedef struct node *nodePointer;
typedef struct node {
	int data;
	nodePointer link;
}node;

void main()
{
	short int out[MAX_SIZE];
	nodePointer seq[MAX_SIZE];
	nodePointer x, y, top;
	FILE* fp = fopen("input.txt", "r");
	int i, j, n;

	fscanf(fp, "%d", &n);
	printf("current size of S : %d\n", n);

	for (i = 0; i < n; i++) {
		out[i] = FALSE;
		seq[i] = NULL;
	}

	printf("S = ( ");
	for (i = 0; i < n; i++) {
		if (i <= 9)
			printf("%3d,", i + 1);
		else if (i == 10)
			printf("%3d", i+1);
		else
			break;
	}
	printf("  )\n");
	
	printf("input pairs : "); //input the equivalence pairs
	
	while (fscanf(fp, "%d %d", &i, &j) != EOF) {
		printf("%dR%d ", i, j);
		x = (nodePointer*)malloc(sizeof(*x));
		x->data = j;
		x->link = seq[i];
		seq[i] = x;
		x = (nodePointer*)malloc(sizeof(*x));
		x->data = i;
		x->link = seq[j];
		seq[j] = x;
	}
	
	printf("\n");
	for (i = 0; i < n; i++) { //output the equivalene classes
		if (out[i] == FALSE) {
			printf("\nNew class: %5d", i);
			out[i] = TRUE;
			x = seq[i]; top = NULL;
			for (;;) {
				while (x) {
					j = x->data;
					if (out[j] == FALSE) {
						printf("%5d", j);
						out[j] = TRUE;
						y = x->link;
						x->link = top;
						top = x;
						x = y;
					}
					else x = x->link;
				}
				if (!top) break;
				x = seq[top->data];
				top = top->link;
			}
		}
	}
	printf("\n");
}