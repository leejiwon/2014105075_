#include <stdio.h>
#include <stdlib.h>
#define MAX_VERTICES 20

typedef struct node *nodePointer;
typedef struct node {
	int vertex;
	nodePointer link;
};

typedef struct {
	int count;
	nodePointer link;
} hdnodes;
hdnodes graph[MAX_VERTICES];

void topSort(hdnodes graph[], int n)
{
	int i, j, k, top;
	nodePointer ptr;

	top = -1;
	for (i = 0; i < n; i++)
	{
		if (!graph[i].count)
		{
			graph[i].count = top;
			top = i;
		}
	}
	for (i = 0; i < n; i++)
	{
		if (top == -1)
		{
			fprintf(stderr, "\nNetwork has a cycle. Sort terminated. \n");
			exit(EXIT_FAILURE);
		}
		else
		{
			j = top;
			top = graph[top].count;
			printf("v%d, ", j);
			for (ptr = graph[j].link; ptr; ptr = ptr->link)
			{
				k = ptr->vertex;
				graph[k].count--;
				if (!graph[k].count)
				{
					graph[k].count = top;
					top = k;
				}
			}
		}
	}
}

void main()
{
	FILE* fp = fopen("input.txt", "r");
	int max_vertex, max_edge, i, j, vertex, edge;

	fscanf(fp, "%d %d", &max_vertex, &max_edge);

	for (i = 0; i < max_vertex; i++)
	{
		graph[i].count = 0;
		graph[i].link = NULL;
	}

	while (fscanf(fp, "%d %d", &vertex, &edge) != EOF)
	{
		nodePointer temp1;
		temp1 = (nodePointer)calloc(1, sizeof(*temp1));

		if (graph[vertex].link == NULL)
		{
			temp1->vertex = edge;
			temp1->link = NULL;
			graph[vertex].link = temp1;
			graph[edge].count++;
		}
		else
		{
			temp1->vertex = edge;
			temp1->link = graph[vertex].link;
			graph[vertex].link = temp1;
			graph[edge].count++;
		}
	}

	topSort(graph, max_vertex);
	printf("\n");
	fclose(fp);
}