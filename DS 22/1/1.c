#include <stdio.h>
#include <stdlib.h>
#define MAX 20

typedef struct {
	int key;
}element;

element* ht[MAX] = { NULL };

int b = 8;
int randNum[MAX];

int h(int k)
{
	return k % b;
}

int s(int num)
{
	return randNum[num];
}

element* search(int k, int *count)
{
	int homeBucket, currentBucket;
	int slot;
	int i = 1;
	homeBucket = h(k);
	for (currentBucket = homeBucket; ht[currentBucket] && ht[currentBucket]->key != k;)
	{
		(*count)++;
		slot = s(i);
		currentBucket = (homeBucket + slot) % b;
		if (currentBucket == homeBucket)
			return NULL;
		i++;
	}
	if (ht[currentBucket] != NULL && ht[currentBucket]->key == k)
	{
		(*count)++;
		return ht[currentBucket];
	}
	return NULL;
}

void insert(element* a)
{
	int homeBucket, currentBucket;
	int k = a->key;
	int slot;
	int i = 1;
	homeBucket = h(k);
	for (currentBucket = homeBucket; ht[currentBucket] && ht[currentBucket]->key != k;)
	{
		slot = s(i);
		currentBucket = (homeBucket + slot) % b;
		if (currentBucket == homeBucket)
		{
			printf("ht is FULL");
			exit(1);
		}
		i++;
	}
	ht[currentBucket] = a;
}

void main()
{
	FILE* fp = fopen("input.txt", "r");
	int seed, i, j, r, count_i = 0, find, count = 0;
	int key[MAX];
	element *hash;

	printf("key sequence from file : ");
	while (fscanf(fp, "%d", &key[count_i]) != EOF)
	{
		printf("%4d", key[count_i]);
		count_i++;
	}
	printf("\n");

	printf("input seed >> ");
	scanf("%d", &seed);
	srand(seed);
	printf("\n");

	for (i = 1; i <= b-1; i++)
	{
	again:;
		r = rand() % 7 + 1;
		for (j = 1; j <= i-1; j++)
		{
			if (r == randNum[j])
			{
				goto again;
			}
		}
		randNum[i] = r;
		printf("randNum[%d] : %3d\n", i, randNum[i]);
	}
	printf("\n           key\n");
	for (i = 0; i < b; i++)
	{
		hash = (element*)malloc(sizeof(element));
		hash->key = key[i];
		insert(hash);
	}

	for (i = 0; i < b; i++)
	{
		if (ht[i] == NULL)
		{
			printf("ht[%2d] :\n", i);
			continue;
		}
		printf("ht[%2d] : %4d\n", i, ht[i]->key);
	}
	printf("\n");

	while (1)
	{
		count = 0;
		printf("input 0 to quit\n");
		printf("key to serch >> ");
		scanf("%d", &find);
		if (find == 0)
		{
			break;
		}

		if (search(find, &count) == NULL)
		{
			printf("It dosen't exist!\n\n");
		}
		else
		{
			printf("key : %d, the number of comparitions : %d\n\n", find, count);
		}
	}

	fclose(fp);
}

