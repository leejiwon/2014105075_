#include <stdio.h>
#include <string.h>
#define FALSE 0
#define TRUE 1

struct person{
	char name[10];
	int age;
	int salary;
};

int humansEqual(struct person person1,struct person person2)
{
	if (strcmp(person1.name, person2.name))
		return FALSE;
	if (person1.age != person2.age)
		return FALSE;
	if (person1.salary != person2.salary)
		return FALSE;
	return TRUE;
}

void main()
{
	struct person person1;
	struct person person2;

	printf("Input person1's name, age, salary : \n");
	scanf("%s", &person1.name);
	fflush(stdin);
	scanf("%d", &person1.age);
	scanf("%d", &person1.salary);
	printf("Input person2's name, age, salary : \n");
	scanf("%s", &person2.name);
	fflush(stdin);
	scanf("%d", &person2.age);
	scanf("%d", &person2.salary);


	if (humansEqual(person1,person2))
	{
		printf("The two human beings are the same\n");
	}
	else
	{
		printf("The two human beings are not the same\n");
	}
}
