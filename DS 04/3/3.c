#include <stdio.h>
#include <stdlib.h>

#define max_string_size 100
#define max_pattern_size 100

int failure[max_pattern_size];
char string[max_string_size];
char pat[max_pattern_size];

int pmatch(char *string, char *pat)
{
	int i = 0, j = 0;
	int lens = strlen(string);
	int lenp = strlen(pat);

	while (i < lens && j < lenp)
	{
		if (string[i] == pat[j])
			i++, j++;
		else if (j == 0)
			i++;
		else
			j = failure[j - 1] + 1;
	}
	return ((j == lenp) ? (i - lenp) : -1);
}

void fail(char *pat)
{
	int i = 0, j;
	int n = strlen(pat);

	failure[0] = -1;

	for (j = 1; j < n; j++)
	{
		i = failure[j - 1];
		while ((pat[j] != pat[i + 1]) && (i >= 0))
			i = failure[i];
		if (pat[j] == pat[i + 1])
			failure[j] = i + 1;
		else
			failure[j] = -1;
	}
}

int main(void)
{
	FILE *fs = fopen("string.txt", "r");
	FILE *fp = fopen("pattern.txt", "r");
	char s[max_string_size];
	char p[max_string_size];
	char temp[max_string_size];
	int i, cnt = 1, PM, PMsum = 0;

	fgets(s, 100, fs);
	fgets(p, 100, fp);

	fclose(fs);
	fclose(fp);

	printf("\t\t012345678901234567890123456789\n");
	printf("string : %s\n", s);
	printf("pattern : %s\n\n", p);

	printf(" j : ");
	for (i = 0; i < strlen(p); i++)
		printf("%3d", i);

	printf("\npat : ");
	for (i = 0; i < strlen(p); i++)
		printf("%3c", p[i]);

	fail(p);

	printf("\n f : ");
	for (i = 0; i < strlen(p); i++)
		printf("%3d", failure[i]);

	printf("\n\n");

	strcpy(temp, s);

	for (i = 0; (PM = pmatch(s, p)) != -1; i++)
	{
		PMsum += PM;
		printf("matching position %d : %d\n", cnt++, PMsum);
		PMsum += strlen(p);
		strncpy(s, s + PM + strlen(p), strlen(s) - PM + strlen(p));
		s[strlen(temp) - PMsum] = '\0';
	}

	printf("\nthe number of pattern matching : %d\n", cnt - 1);

	return 0;
}