#include <stdio.h>

void init2dArray(int ary[][4], int rows, int cols)
{
	int i, j, num;

	for (i = 0; i < 3; i++)
	{
		num = i;
		for (j = 0; j < 4; j++)
		{
			ary[i][j] = num++;
		}
	}
}

void print2dArray(int *pary, int rows, int cols)
{
	int i, j;

	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 4; j++)
		{
			printf("%d ",(*pary+i)+j);
		}
		printf("\n");
	}
}

void main()
{
	int ary[3][4];
	int *pary = ary;

	init2dArray(ary, 3, 4);
	print2dArray(pary, 3, 4);
	printf("\n");
}