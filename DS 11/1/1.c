#include <stdio.h>
#include <stdlib.h>
#define MAX_STACK_SIZE 100
#define MAX_QUEUE_SIZE 100

typedef struct node *treePointer;
typedef struct node {
	char data;
	treePointer leftChild, rightChild;
}node;
treePointer root;
treePointer stack[MAX_STACK_SIZE];
int top = -1;
treePointer queue[MAX_QUEUE_SIZE];
int front = 0, rear = 0;
typedef enum { lparen, rparen, plus, minus, times, divide, mod, eos, operand } precedence;
char expr[81];

void push(treePointer item)
{
	stack[++top] = item;
}

treePointer stackEmpty()
{
	return NULL;
}

treePointer pop()
{
	if (top == -1)
		return stackEmpty();
	return stack[top--];
}

precedence getToken(char *symbol, int *n)
{
	*symbol = expr[(*n)++];
	switch (*symbol) {
	case '(': return lparen;
	case ')': return rparen;
	case '+': return plus;
	case '-': return minus;
	case '/': return divide;
	case '*': return times;
	case '%': return mod;
	case '\0': return eos;
	default: return operand;
	}
}

treePointer eval(void)
{
	precedence token;
	treePointer tp, tp2;

	char symbol;
	int n = 0;

	token = getToken(&symbol, &n);

	while (token != eos) {
		if (token == operand) {
			tp = (treePointer)malloc(sizeof(*tp));
			tp->rightChild = NULL;
			tp->leftChild = NULL;
			tp->data = expr[n - 1];
			push(tp);
		}
		else {
			tp2 = (treePointer)malloc(sizeof(*tp2));
			tp2->rightChild = pop();
			tp2->leftChild = pop();

			switch (token) {
			case plus:
				tp2->data = '+';
				push(tp2);
				break;
			case minus:
				tp2->data = '-';
				push(tp2);
				break;
			case times:
				tp2->data = '*';
				push(tp2);
				break;
			case divide:
				tp2->data = '/';
				push(tp2);
				break;
			case mod:
				tp2->data = '%';
				push(tp2);
				break;
			}
		}
		token = getToken(&symbol, &n);
	}
	return stack[0];
}

void iterInorder(treePointer node)
{
	top = -1;
	for (;;) {
		for (; node; node = node->leftChild)
			push(node);
		node = pop();
		if (!node)
			break;
		printf("%c", node->data);
		node = node->rightChild;
	}
}

void addq(treePointer item)
{
	rear = (rear + 1) % MAX_QUEUE_SIZE;
	queue[rear] = item;
}

treePointer queueEmpty()
{
	return NULL;
}

treePointer deleteq()
{
	treePointer item;
	if (front == rear)
		return queueEmpty();
	front = (front + 1) % MAX_QUEUE_SIZE;
	return queue[front];
}

void levelOrder(treePointer ptr)
{
	if (!ptr)
		return;
	addq(ptr);
	for (;;) {
		ptr = deleteq();
		if (ptr) {
			printf("%c", ptr->data);
			if (ptr->leftChild)
				addq(ptr->leftChild);
			if (ptr->rightChild)
				addq(ptr->rightChild);
		}
		else break;
	}
}

void main()
{
	FILE* fp = fopen("input.txt", "r");
	char printchar;
	int i = 0;

	printf("the length of input string should be less than 80\n");
	printf("input string <postfix expression> : ");
	
	while (fscanf(fp, "%c", &printchar) != EOF) {
		expr[i] = printchar;
		i++;
	}
	expr[i] = '\0';
	puts(expr);

	printf("creating its binary tree\n\n");
	root = eval();

	printf("iterative inorder traversal       : ");
	iterInorder(root);
	printf("\n");

	printf("level order traversal             : ");
	levelOrder(root);
	printf("\n");
}