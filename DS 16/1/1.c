#include <stdio.h>
#include <stdlib.h>
#define FALSE 0
#define TRUE 1
#define MAX_VERTICES 20

typedef struct node *nodePointer;
typedef struct node {
	int vertex;
	nodePointer link;
}nodelist;
short int visited[MAX_VERTICES];
nodePointer graph[MAX_VERTICES];

void dfs(int v)
{
	nodePointer w;
	visited[v] = TRUE;
	printf("%5d", v);
	for (w = graph[v]->link; w; w = w->link)
	{
		if (!visited[w->vertex])
			dfs(w->vertex);
	}
}

void main()
{
	FILE* fp = fopen("input.txt", "r");
	int max_vertex, max_edge, i, j, vertex, edge;
	nodePointer tp[MAX_VERTICES];
	fscanf(fp, "%d %d", &max_vertex, &max_edge);

	for (i = 0; i < max_vertex; i++)
	{
		graph[i] = (nodePointer)calloc(1, sizeof(*tp[i]));
	}

	for (i = 0; i < max_vertex; i++)
	{
		tp[i] = (nodePointer)calloc(1, sizeof(*tp[i]));
	}

	while (fscanf(fp, "%d %d", &vertex, &edge) != EOF)
	{
		nodePointer temp1, temp2;
		temp1 = (nodePointer)calloc(1, sizeof(*temp1));
		temp2 = (nodePointer)calloc(1, sizeof(*temp2));

		if (graph[vertex]->link == NULL)
		{
			temp1->vertex = edge;
			temp1->link = NULL;
			graph[vertex]->link = temp1;
		}
		else
		{
			temp1->vertex = edge;
			temp1->link = graph[vertex]->link;
			graph[vertex]->link = temp1;
		}

		if (graph[edge] == NULL)
		{
			temp2->vertex = vertex;
			temp2->link = NULL;
			graph[edge]->link = temp2;
		}
		else
		{
			temp2->vertex = vertex;
			temp2->link = graph[edge]->link;
			graph[edge]->link = temp2;
		}
	}

	for (i = 0; i < max_vertex; i++)
	{
		tp[i] = graph[i];
	}
	
	printf("<<<<<<<<<<<< Adjacency List >>>>>>>>>>>>>>\n");
	for (i = 0; i < max_vertex; i++)
	{
		printf("adjList[%d] : ", i);
		for (j = 0; j < max_vertex;j++)
		{
			tp[i] = tp[i]->link;
			if (tp[i] == NULL)
			{
				break;
			}
			printf("%5d", tp[i]->vertex);
		}
		printf("\n");
	}

	printf("\n<<<<<<<<<<<< Depth First Search >>>>>>>>>>>>>>\n");
	for (i = 0; i < max_vertex; i++)
	{
		dfs(i);
		for (j = 0; j < max_vertex; j++)
		{
			visited[j] = 0;
		}
		printf("\n");
	}

}