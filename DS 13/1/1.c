#include <stdio.h>
#include <stdlib.h>
#define MAX_ELEMENTS 200
#define HEAP_FULL(n) (n==MAX_ELEMENTS-1)
#define HEAP_EMPTY(n) (!n)

typedef struct {
	int key;
} element;
element heap[MAX_ELEMENTS];
int n = 0;

void main()
{
	int seed, k, key, size;
	int i;

	printf("Seed k : ");
	scanf("%d %d", &seed, &k);
	srand(seed);
	printf("insert random number\n");
	for(i = k; i < k*2; i++) {
		key = rand()%100 +1;
		heap[i].key = key;
		printf("%4d", heap[i].key);
	}
	printf("\n");
	size = k*2;

	while(k != 0) {
		for(i = k; i < k*2; i += 2) {
			if(heap[i].key < heap[i+1].key)
				heap[i/2].key = heap[i].key;
			else
				heap[i/2].key = heap[i+1].key;
		}
		k /= 2;
	}

	printf("The initial winner tree includes followings : \n");
	for(i = 1; i < size; i++) {
		printf("%4d",heap[i].key);
	}
	printf("\n");
}