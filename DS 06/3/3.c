// 2014105075 이지원
// 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#include <stdio.h>
#include <stdlib.h>
#define MAX_STACK_SIZE 20
#define MAX_EXPR_SIZE 20

typedef enum {
	lparen,
	rparen,
	plus,
	minus,
	times,
	divide,
	mod,
	eos,
	operand
} precedence;

int isp[] = { 0, 19, 12, 12, 13, 13, 13, 0 };
int icp[] = { 20, 19, 12, 12, 13, 13, 13, 0 };

FILE *f2;
precedence stack[MAX_STACK_SIZE];
char expr[MAX_EXPR_SIZE];
int top = -1;

void push(precedence item)
{
	stack[++top] = item;
}

precedence pop()
{
	return stack[top--];
}

precedence gettoken(char *symbol, int *n)
{
	*symbol = expr[(*n)++];
	switch (*symbol) {
	case '(': return lparen;
	case ')': return rparen;
	case '+': return plus;
	case '-': return minus;
	case '*': return times;
	case '/': return divide;
	case '%': return mod;
	case '\0': return eos;
	default: return operand;
	}

}

void printtoken(precedence item1)
{
	char a;

	switch (item1)
	{
	case 2:
		a = '+';
		break;
	case 3:
		a = '-';
		break;
	case 4:
		a = '*';
		break;
	case 5:
		a = '/';
		break;
	case 6:
		a = '%';
		break;
	}

	fprintf(f2, "%c", a);
	printf("%c", a);
}

void postfix(void)
{
	char symbol;
	precedence token;
	int n = 0;
	top = 0;
	stack[0] = eos;

	for (token = gettoken(&symbol, &n); token != eos; token = gettoken(&symbol, &n)) {

		if (token == operand) {
			printf("%c", symbol);
			fprintf(f2, "%c", symbol);
		}

		else if (token == rparen) {
			while (stack[top] != lparen) {
				printtoken(pop());
			}
			pop();
		}

		else {
			while (isp[stack[top]] >= icp[token]) {
				printtoken(pop());
			}
			push(token);
		}
	}

	while ((token = pop()) != eos) {
		printtoken(token);
	}
	printf("\n");
}

void main()
{
	int i;
	FILE *f = fopen("input3.txt", "r");
	f2 = fopen("output3.txt", "w");
	printf("<<<<<<<<< infix to postfix >>>>>>>>>\n");
	printf("infix expression        : ");

	for (i = 0; i < MAX_EXPR_SIZE; i++) {
		fscanf(f, "%c", &expr[i]);
	}

	for (i = 0; i < MAX_EXPR_SIZE; i++) {
		printf("%c", expr[i]);
	}

	printf("\npostfix expresstion     : ");

	postfix();
	printf("\n");

	fclose(f);
	fclose(f2);
}