#include <stdio.h>
#include <math.h>
#include "SelectionSort.h"

void swap(int *x, int *y)
{
	int temp = *x;
	*x = *y;
	*y = temp;
}

void sort(int list[], int n)
{
	int i, j, min1;
	for (i = 0; i < n - 1; i++)
	{
		min1 = i;
		for (j = i + 1; j < n; j++)
		{
			if (list[j]< list[min1])
			{
				min1 = j;
			}

		}swap(&list[i], &list[min1]);
	}
}