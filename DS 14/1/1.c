#include <stdio.h>
#include <stdlib.h>
#define MAX_VERTEX 10

typedef struct node *nodePointer;
typedef struct node {
	int data;
	nodePointer link;
}node;
nodePointer list[MAX_VERTEX];

void main()
{
	int i, j, vertex1, edge1, vertex, edge;
	FILE *f = fopen("input.txt", "r");

	fscanf(f, "%d %d", &vertex1, &edge1);

	for (i = 0; i < vertex1; i++) {
		list[i] = (nodePointer)calloc(1, sizeof(*list[i]));
	}

	while (fscanf(f, "%d %d", &vertex, &edge) != EOF) {
		nodePointer temp1, temp2;
		temp1 = (nodePointer)calloc(1, sizeof(*temp1));
		temp2 = (nodePointer)calloc(1, sizeof(*temp2));

		if (list[vertex]->link == NULL) {
			temp1->data = edge;
			temp1->link = NULL;
			list[vertex]->link = temp1;
		}
		else {
			temp1->data = edge;
			temp1->link = list[vertex]->link;
			list[vertex]->link = temp1;
		}
		if (list[edge]->link == NULL) {
			temp2->data = vertex;
			temp2->link = NULL;
			list[edge]->link = temp2;
		}
		else {
			temp2->data = vertex;
			temp2->link = list[edge]->link;
			list[edge]->link = temp2;
		}
	}

	printf("< graph - adjacency list representation >\n");

	for (i = 0; i < vertex1; i++) {
		printf("egdes incident to vertex %d : ", i);
		for (j = 0; j < vertex1; j++) {
			list[i] = list[i]->link;
			if (list[i] == NULL) {
				break;
			}
			printf(" (%2d, %2d) ", i, list[i]->data);
		}
		printf("\n");
	}
}