// 2014105075 이지원
// 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#include <stdio.h>
#include <stdlib.h>
#define MAX_EXPR_SIZE 9

char expr[MAX_EXPR_SIZE];
int top = -1;

typedef enum {
	lparen,
	rparen,
	plus,
	minus,
	times,
	divide,
	mod,
	eos,
	operand
} precedence;

void push(int item)
{
	expr[++top] = item;
}

int pop()
{
	return expr[top--];
}

precedence gettoken(char *symbol, int *n)
{
	*symbol = expr[(*n)++];
	switch (*symbol) {
	case '(': return lparen;
	case ')': return rparen;
	case '+': return plus;
	case '-': return minus;
	case '/': return divide;
	case '*': return times;
	case '%': return mod;
	case '\0': return eos;
	default: return operand;
	}

}

int eval(void)
{
	precedence token;
	char symbol;
	int op1, op2;
	int n = 0;
	token = gettoken(&symbol, &n);
	
	while (token != eos) {
		if (token == operand) {
			push(symbol - '0');
		}
		else {
			op2 = pop();
			op1 = pop();

			switch (token) {
			case plus: push(op1 + op2);
				break;
			case minus: push(op1 - op2);
				break;
			case times: push(op1*op2);
				break;
			case divide: push(op1 / op2);
				break;
			case mod: push(op1%op2);
			}
		}
		token = gettoken(&symbol, &n);
	}
	return pop();
}

void main()
{
	int i;
	FILE *f = fopen("input.txt", "r");

	printf("postfix expresstion : ");

	for (i = 0; i < MAX_EXPR_SIZE; i++) {
		fscanf(f, "%c", &expr[i]);
	}

	for (i = 0; i < MAX_EXPR_SIZE; i++) {
		printf("%c", expr[i]);
	}
	printf("\nthe evaluation value : %d\n",eval());
	fclose(f);
}