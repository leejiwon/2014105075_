#include <stdio.h>
#define MAX_SIZE 20

typedef struct element{
	int key;
}element;

element a[MAX_SIZE];
int link[MAX_SIZE];

int rmergeSort(element a[], int link[], int left, int right)
{
	int mid = (left + right) / 2;

	if (left >= right) return left;

	return listMerge(a,link,rmergeSort(a,link,left,mid),rmergeSort(a,link,mid+1,right));
}

int listMerge(element a[], int link[], int start1, int start2)
{
	int last1,last2,lastResult = 0;
	for(last1 = start1, last2 = start2; last1 && last2;)
		if(a[last1].key <= a[last2].key){
			link[lastResult] = last1;
			lastResult = last1;
			last1 = link[last1];
		}
		else{
			link[lastResult] = last2;
			lastResult = last2;
			last2 = link[last2];
		}

		if (last1 == 0) link[lastResult] = last2;
		else link[lastResult] = last1;
		return link[0];
}

void printMerge(element a[],int n)
{
	int i;

	for(i=1;i<=n;i++)
		printf("%d ",a[i].key);
}

void main()
{
	FILE *fi = fopen("input.txt","r"),*fo = fopen("output.txt","w");

	int i = 1;
	int n = 1;
	int linkKey=0;
	element printa[MAX_SIZE];

	while(fscanf(fi,"%d",&a[n].key) != EOF){
		n++;
	}
	n--;
	rmergeSort(a,link,1,n);

	while(i <= n){
		fprintf(fo,"%d ",a[link[linkKey]].key);
		printa[i].key = a[link[linkKey]].key;
		linkKey = link[linkKey];
		i++;
	}

	printf("a = ");
	printMerge(printa,n);
	printf("\n");

	fclose(fi);
	fclose(fo);
}