//2014105075 이지원
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#include <stdio.h>
#include <stdlib.h>

typedef struct node *nodePointer;
typedef struct node {
	nodePointer llink;
	int data;
	nodePointer rlink;
}node;
nodePointer head = NULL;


void dinsert(nodePointer node, nodePointer newnode)
{/* insert newnode to the right of node */
	//	newnode->data = n;
	newnode->llink = node;
	newnode->rlink = node->rlink;
	node->rlink->llink = newnode;
	node->rlink = newnode;
}

void ddelete(nodePointer node, nodePointer deleted)
{/* delete from the doubly linked list */
	if (node == deleted)
		printf("Deletion of header node not permitted.\n");
	else{
		deleted->llink->rlink = deleted->rlink;
		deleted->rlink->llink = deleted->llink;
		free(deleted);
	}
}
void printLlink(nodePointer node)
{
	nodePointer a;
	a = node;
	a = a->llink;
	int i = 0;
	printf("\nbackward\n");
	while (a != node)
	{
		if (i % 10 == 0 && i != 0)
			printf("\n");
		printf("%d ", a->data);
		a = a->llink;
		i++;
	}
	printf("\n");
}
void printRlink(nodePointer node)
{
	nodePointer a;
	a = node;
	a = a->rlink;
	int i = 0;
	printf("forward\n");
	while (a != node)
	{
		if (i % 10 == 0 && i != 0)
			printf("\n");
		printf("%d ", a->data);
		a = a->rlink;
		i++;
	}
	printf("\n");
}
void main()
{
	int n = 0;
	FILE *fp = fopen("input.txt", "r");
	nodePointer new1, a2;

	head = (nodePointer)malloc(sizeof(*head));
	a2 = (nodePointer)malloc(sizeof(*a2));
	a2 = head;
	//	emp->llink = emp;
	//	emp->rlink = emp;
	//	head = emp;
	head->llink = head;
	head->rlink = head;
	while (fscanf(fp, "%d", &n) != EOF)
	{
		new1 = (nodePointer)malloc(sizeof(*new1));
		new1->data = n;
		dinsert(head->llink, new1);
	}
	printf("After creating a doubly linked circular list with a head node : \n");

	printRlink(head);
	printLlink(head);

	while (1)
	{

		a2 = a2->llink;

		if (a2 == head)
			break;
		if (a2->data <= 50)
		{
			ddelete(head, a2);
			a2 = head;
		}
	}
	printf("\nAfter deleting numbers less than and equal to 50 :\n");
	printRlink(head);
	printLlink(head);

	printf("\nAfter deleting all nodes except for the header node :\n");
	a2 = head;
	while (1)
	{
		a2 = a2->llink;
		if (a2 == head)
			break;
		ddelete(head, a2);
		a2 = head;
	}
	printRlink(head);
	printLlink(head);

}