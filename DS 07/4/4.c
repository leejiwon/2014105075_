/*
2014105075 이지원
본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
*/

#include <stdio.h>
#include <stdlib.h>

#define MAX_QUEUES 3

typedef struct
{
	int id;
	int grade;
} element;
typedef struct queue *queuePointer;
typedef struct queue
{
	element data;
	queuePointer link;
} Node;
queuePointer front[MAX_QUEUES], rear[MAX_QUEUES];

element queueEmpty();
void addq(int i, element item);
element deleteq(int i);

int main(void)
{
	FILE *f = fopen("input.txt", "r");
	element item;
	int i, id, grade;

	while (!feof(f))
	{
		fscanf(f, "%d %d %d", &i, &item.id, &item.grade);
		addq(i, item);
	}

	printf("과목번호, 학번, 성적\n");

	for (i = 0; i < MAX_QUEUES; i++)
	{
		printf("**********************\n");
		while (1)
		{
			item = deleteq(i);
			if (item.id == -1)
				break;
			printf("%8d %5d %5d\n", i, item.id, item.grade);
		}
	}

	return 0;
}

element queueEmpty()
{
	element temp;

	temp.id = -1;

	return temp;
}

element deleteq(int i)
{
	queuePointer temp = front[i];
	element item;

	if (!temp)
		return queueEmpty();

	item = temp->data;
	front[i] = temp->link;
	free(temp);

	return item;
}

void addq(int i, element item)
{
	queuePointer temp;

	temp = (queuePointer) malloc(sizeof(*temp));
	temp->data = item;
	temp->link = NULL;

	if (front[i])
		rear[i]->link = temp;
	else
		front[i] = temp;

	rear[i] = temp;
}