#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_QUEUE_SIZE 5
#define MAX_NAME_SIZE 20

typedef struct {
	int no;
	char name[MAX_NAME_SIZE];
}element;

element queue[MAX_QUEUE_SIZE];
int rear = -1;
int front = -1;

void queueFull()
{
	int i,j;
	if (front == -1) {
		printf("Qoueue is full, cannot add element!\n");		
		for (i = 0; i <= MAX_QUEUE_SIZE-1; i++) {
			printf("%d %s\n", queue[i].no, queue[i].name);
		}
		exit(EXIT_FAILURE);
	}
	else {
		for (i = front + 1, j = 0; i <= rear; i++, j++) {
			queue[j] = queue[i];
		}
		front = -1;
		rear = j - 1;
	}
}

element queueEmpty()
{
	printf("Qoueue is empty, cannot add element!\n");
	exit(EXIT_FAILURE);
}

void addq(element item)
{	
	if (rear == MAX_QUEUE_SIZE - 1)
		queueFull();
	queue[++rear] = item;
}

element deleteq()
{
	if (front == rear)
		return queueEmpty();
	return queue[++front];
}

void main()
{
	char line[100], oper[5];

	element item2;

	printf("<< stack operations where MAX_QUEUE_SIZE is %d >>\n", MAX_QUEUE_SIZE);
	printf("**************************************************\n");

	while (1)
	{
		gets(line);

		sscanf(line, "%s %d %s", oper, &item2.no, item2.name);

		if (strcmp(oper,"a")==0) {
			addq(item2);
		}
		else if (strcmp(oper,"d")==0) {
			deleteq();
		}
	}
}