#include <stdio.h>
#include <stdlib.h>

#define MAX_STRLEN 100

int nfind(char *string, char *pat)
{
	int i, j = 0, start = 0;
	int lasts = strlen(string) - 1;
	int lastp = strlen(pat) - 1;
	int endmatch = lastp;

	for (i = 0; endmatch <= lasts; endmatch++, start++)
	{
		if (string[endmatch] == pat[lastp])
			for (j = 0, i = start; j < lastp && string[i] == pat[j]; i++, j++);
		if (j == lastp)
			return start;
	}

	return -1;
}



int main(void)
{
	FILE *fs = fopen("string.txt", "r");
	FILE *fp = fopen("pattern.txt", "r");
	char s[MAX_STRLEN];
	char p[MAX_STRLEN];
	char temp[MAX_STRLEN];
	int i, cnt = 1, NF, NFsum = 0;

	fgets(s, 100, fs);
	fgets(p, 100, fp);

	fclose(fs);
	fclose(fp);

	printf("\t\t012345678901234567890123456789\n");
	printf("string : %s\n", s);
	printf("pattern : %s\n\n", p);

	strcpy(temp, s);

	for (i = 0; (NF = nfind(s, p)) != -1; i++)
	{
		NFsum += NF;
		printf("matching position %d : %d\n", cnt++, NFsum);
		NFsum += strlen(p);
		strncpy(s, s + NF + strlen(p), strlen(s) - NF + strlen(p));
		puts(s);
		s[strlen(temp) - NFsum] = '\0';
	}

	printf("\nthe number of pattern matching : %d\n", cnt - 1);

	return 0;
}