#include <stdio.h>
#include <stdlib.h>

int** make2dArray(int rows, int cols)
{
	int i;
	int **x;

	x=(int**)malloc(rows*sizeof(*x));

	for (i = 0; i < rows; i++)
	{
		x[i]=(int*)malloc(cols*sizeof(**x));
	}

	return x;
}

void init2dArray(int **ary, int rows, int cols)
{
	int i, j, num;

	for (i = 0; i < rows; i++)
	{
		num = i;
		for (j = 0; j < cols; j++)
		{
			ary[i][j] = num++;
		}
	}
}

void print2dArray(int **ary, int rows, int cols)
{
	int i, j;

	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < cols; j++)
		{
			printf("%d ", ary[i][j]);
		}
		printf("\n");
	}
}

void free2dArray(int **ary, int rows)
{
	int i;

	for (i = 0; i < rows; i++)
	{
		free(ary[i]);
	}

	free(ary);
}

void main()
{
	int **ary;
	int rows, cols;

	printf("Enter row size and column size (ex) 3 4 : ");
	scanf("%d %d", &rows, &cols);

	ary = make2dArray(rows, cols);

	init2dArray(ary, rows, cols);
	print2dArray(ary, rows, cols);

	free2dArray(ary, rows);

	printf("\n2d array - free !!!\n");
}