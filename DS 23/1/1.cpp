#include <stdio.h>
#include <stdlib.h>
#define SWAP(x, y, t) (t=x, x=y, y=t)

typedef struct{
	int key;
}element;

typedef struct leftist *leftistTree;
typedef struct leftist{
	leftistTree leftChild;
	element data;
	leftistTree rightChild;
	int shortest;
}leftist;

int front = 0;
int rear = 0;

leftistTree *queue = NULL;
int capacity = 2;

void minUnion(leftistTree *a, leftistTree *b)
{
	leftistTree temp;
	if ((*a)->data.key > (*b)->data.key) SWAP(*a, *b, temp);

	if (!(*a)->rightChild) (*a)->rightChild = *b;
	else minUnion(&(*a)->rightChild, b);

	if (!(*a)->leftChild)
	{
		(*a)->leftChild = (*a)->rightChild;
		(*a)->rightChild = NULL;
	}
	else if ((*a)->leftChild->shortest < (*a)->rightChild->shortest)
		SWAP((*a)->leftChild, (*a)->rightChild, temp);
	(*a)->shortest = (!(*a)->rightChild) ? 1 : (*a)->rightChild->shortest + 1;
}

void minMeld(leftistTree *a, leftistTree *b)
{
	if (!*a) *a = *b;
	else if (*b) minUnion(a, b);
	*b = NULL;
}

void inorder(leftistTree a)
{
	if (a)
	{
		inorder(a->leftChild);
		printf("%3d ", a->data.key);
		inorder(a->rightChild);
	}
}

void insert(leftistTree a)
{
	minMeld(queue, &a);
}

void copy(leftistTree *a, leftistTree *b, leftistTree *c)
{
	int i = 0;
	for (i = 0; i < b - a; i++)
	{
		c[i] = a[i];
	}
}

leftistTree queueEmpty()
{
	leftistTree item;
	item->data.key = -1;
	return item;
}

void queueFull()
{
	int start;
	leftistTree *newQueue;
	newQueue = (leftistTree* )malloc(2 * capacity*sizeof(leftistTree));

	start = (front + 1) % capacity;
	rear--;

	if (start < 2) {
		copy(queue + start, queue + start + capacity - 1, newQueue);
	}
	else {
		copy(queue + start, queue + capacity, newQueue);
		copy(queue, queue + rear + 1, newQueue + capacity - start);
	}

	front = 2 * capacity - 1;
	rear = capacity - 1;
	capacity *= 2;
	free(queue);
	queue = newQueue;
}

void addq(leftistTree item)
{
	
	rear = (rear + 1) % capacity;
	if (front == rear) {
		queueFull();
	}
	queue[rear] = item;
}

leftistTree deleteq()
{
	if (front == rear) {
		return queueEmpty();
	}
	front = (front + 1) % capacity;

	return queue[front];
}

void deleteF()
{
	leftistTree item;
	item = *queue;
	
	
	if (item->rightChild)
	{
		if (item->rightChild->leftChild == NULL || item->rightChild->rightChild == NULL)
			item->rightChild->leftChild = item->leftChild;
		*queue = item->rightChild;
	}
	else if (item->leftChild)
	{
		if (item->leftChild->leftChild == NULL || item->leftChild->rightChild == NULL)
			item->leftChild->leftChild = item->leftChild;
		*queue = item->leftChild;
	}	
}

void initialize()
{
	leftistTree a, b;

	a = deleteq();
	b = deleteq();

	while (front != rear)
	{
		minMeld(&a, &b);
		addq(a);
		a = deleteq();
		b = deleteq();
	}
	minMeld(&a, &b);
	addq(a);	
}

void main()
{
	int temp;
	int num;
	int i;
	queue = (leftistTree*)malloc(capacity*sizeof(leftistTree));
	leftistTree leftistTemp;
	FILE *fp = fopen("input.txt", "r");
	
	while (fscanf(fp, "%d", &temp) != EOF)
	{
		leftistTemp = (leftistTree)malloc(sizeof(leftist));
		leftistTemp->shortest = 1;
		leftistTemp->leftChild = leftistTemp->rightChild = NULL;
		leftistTemp->data.key = temp;
		addq(leftistTemp);
	}

	printf("초기값\n");
	for (i = 0; i <= rear; i++)
	{
		printf("%3d ", queue[i]->data.key);
	}

	printf("\n\n초기화\n");
	initialize();
	inorder(*queue);
	printf("\n");

	deleteF();
	printf("\n삭제\n");
	inorder(*queue);

	printf("\n\n임의의 정수 추가 : ");
	scanf("%d", &num);

	leftistTemp = (leftistTree)malloc(sizeof(leftist));
	leftistTemp->shortest = 1;
	leftistTemp->leftChild = leftistTemp->rightChild = NULL;
	leftistTemp->data.key = num;
	insert(leftistTemp);

	printf("\n결과");
	inorder(*queue);
	printf("\n");
}