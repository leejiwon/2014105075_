#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_b 11

typedef struct {
	char item[10];
	int key;
}element;

int b = 11;
int s = 1;

element* ht[MAX_b];

int h(int k)
{
	return k % b;
}

unsigned int stringToInt(char *key)
{
	int number = 0;
	while (*key)
		number += *key++;
	return number;
}

element* search(int k, int *count)
{

	int homeBucket, currentBucket;
	homeBucket = h(k);
	for (currentBucket = homeBucket; ht[currentBucket] && ht[currentBucket]->key != k;)
	{
		(*count)++;
		currentBucket = (currentBucket + 1) % b;
		if (currentBucket == homeBucket)
			return NULL;
	}
	if (ht[currentBucket] != NULL && ht[currentBucket]->key == k)
	{
		(*count)++;
		return ht[currentBucket];
	}
	return NULL;
}

void insert(element* a)
{
	int homeBucket, currentBucket;
	int k = a->key;
	homeBucket = h(k);
	for (currentBucket = homeBucket; ht[currentBucket] && ht[currentBucket]->key != k;)
	{
		currentBucket = (currentBucket + 1) % b;
		if (currentBucket == homeBucket)
		{
			printf("ht is FULL");
			exit(2);
		}
	}
	ht[currentBucket] = a;
}

void main()
{
	FILE* fp = fopen("input.txt", "r");
	int i,j=0;
	char data[10];
	char find[10];
	int keynum;
	int findnum;
	int count = 0;
	element *hash;
	
	printf("input strings : ");
	while (fscanf(fp, "%s", data) != EOF)
	{
		keynum = stringToInt(data);
		hash = (element*)malloc(sizeof(element));
		hash->key = keynum;
		strcpy(hash->item, data);
		insert(hash);

		printf("%s ", data);
	}
	
	printf("\n\n		item	key\n");

	for (int i = 0; i < MAX_b; i++)
	{
		if (ht[i] == NULL)
		{
			printf("ht[%2d] :\n", i);
			continue;
		}

		printf("ht[%2d] :	%s	%d\n", i, ht[i]->item, ht[i]->key);
	}

	printf("\n\nstring to search >> ");
	scanf("%s", find);
	findnum = stringToInt(find);

	if (search(findnum, &count) == NULL)
	{
		printf("dosen't exist\n");
		exit(1);
	}
	else
	{
		printf("item : %s, key : %d, the number of comparitions : %d\n", find, findnum, count);
	}
}