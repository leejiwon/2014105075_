#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef int iType;
typedef struct {
	int key;
	iType item;
} element;

typedef struct node *treePointer;
typedef struct node {
	element data;
	treePointer LC, RC;
} node;

treePointer a = NULL;

element* isearch(treePointer root, int k)
{
	if (!root) {
		return NULL;
	}
	if (k == root->data.key) {
		return &(root->data);
	}
	if (k < root->data.key) {
		return isearch(root->LC, k);
	}
	return isearch(root->RC, k);
}

treePointer modifiedSearch(treePointer tree, int k)
{
	while (tree != NULL) {
		if (k == tree->data.key) {
			return NULL;
		}
		if (k < tree->data.key) {
			if (!(tree->LC)) {
				return tree;
			}
			tree = tree->LC;
		}
		else {
			if (!(tree->RC)) {
				return tree;
			}
			tree = tree->RC;
		}
	}
	return NULL;
}

void insert(treePointer *node, int k, iType theItem)
{
	treePointer ptr, temp = modifiedSearch(*node, k);
	if (temp || !(*node)) {
		ptr = (treePointer)malloc(sizeof(*ptr));
		ptr->data.key = k;
		ptr->data.item = theItem;
		ptr->LC = ptr->RC = NULL;

		if (*node) {
			if (k < temp->data.key) {
				temp->LC = ptr;
			}
			else {
				temp->RC = ptr;
			}
		}
		else {
			*node = ptr;
		}
	}
}

void inorder(treePointer ptr)
{
	if (ptr) {
		inorder(ptr->LC);
		printf("%4d", ptr->data.key);
		inorder(ptr->RC);
	}
}

void main()
{
	int i, size, seed, random, srch;
	printf("Random number generation <1 ~ 500>\n");
	printf("The number of nodes in BST <less than 50> : ");
	scanf("%d", &size);
	printf("Seed : ");
	scanf("%d", &seed);
	srand(seed);

	printf("Creating a BST from random numbers\n");

	for (i = 0; i < size; i++) {
		random = rand() % 500 + 1;
		insert(&a, random, random);
	}

	printf("The key to search : ");
	scanf("%d", &srch);
	if (isearch(a, srch) == NULL) {
		printf("\nThere is no such element\n");
	}
	else {
		printf("\nFind\n");
	}

	printf("\nInorder traversal of the BST shows the sorted sequence\n");
	inorder(a);
	printf("\n");
}