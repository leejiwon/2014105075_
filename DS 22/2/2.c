#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define STRING_MAX 11
#define BUCKET_SIZE 11

typedef struct {
	char item[10];
	int key;
}element;

typedef struct node *nodePointer;
typedef struct node {
	element data;
	nodePointer *link;
}node;

node *ht[BUCKET_SIZE] = { NULL };
int b = 11;

unsigned int stringToInt(char *key)
{
	int number = 0;
	while (*key)
		number += *key++;
	return number;
}

int h(int k)
{
	return k % b;
}

element* search(int k, int *count)
{
	node *current;
	int homeBucket = h(k);

	for (current = ht[homeBucket]; current; current = current->link)
	{
		(*count)++;
		if (current->data.key == k)
			return current;
	}
	return NULL;
}

void insert(node *a)
{
	node *current;
	int k = a->data.key;
	int homeBucket = h(k);
	a->link = NULL;

	if (ht[homeBucket] == NULL)
	{
		ht[homeBucket] = a;
	}
	else
	{
		for (current = ht[homeBucket]; current->link; current = current->link);
		current->link = a;
	}
}

void main()
{
	FILE* fp = fopen("input.txt", "r");
	char temp[STRING_MAX], find[STRING_MAX];
	int keynum, i, findkey, count = 0;
	node *hash, *print;

	printf("input strings : ");
	while (fscanf(fp, "%s", temp) != EOF)
	{
		printf("%s ", temp);
		keynum = stringToInt(temp);

		hash = (node*)malloc(sizeof(node));
		hash->data.key = keynum;
		strcpy(hash->data.item, temp);

		insert(hash);
	}
	printf("\n");

	printf("\n\n		item	key\n");
	for (i = 0; i < BUCKET_SIZE; i++)
	{
		if (ht[i] == NULL)
		{
			printf("ht[%2d] :\n", i);
			continue;
		}
		else
		{
			printf("ht[%2d] : ", i);
			for (print = ht[i]; print; print = print->link)
			{
				printf("<%s	%d> ", print->data.item, print->data.key);
			}
			printf("\n");
		}
	}
	printf("\n");

	while (1)
	{
		count = 0;
		printf("input ^Z to quit\n");
		printf("starting to search >> ");
		if (scanf("%s", find) != -1)
		{
			findkey = stringToInt(find);
			print = search(findkey, &count);

			if (print == NULL)
			{
				printf("It dosen't exist!\n\n");
			}
			else
			{
				printf("item : %s, key : %d, the number of comparitions : %d\n\n", find, findkey, count);
			}
		}
		else
			break;
	}
}