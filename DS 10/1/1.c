#include <stdio.h>
#include <stdlib.h>
#define MAX_STACKS 100

typedef struct node *treePointer;
typedef struct node {
	char c;
	treePointer leftChild, rightChild;
}node;
treePointer root;
treePointer stack[MAX_STACKS];
typedef enum { lparen, rparen, plus, minus, times, divide, mod, eos, operand } precedence;
char expr[81];
int top = -1;

void inorder(treePointer ptr)
{
	if (ptr) {
		inorder(ptr->leftChild);
		printf("%c", ptr->c);
		inorder(ptr->rightChild);
	}
}

void preorder(treePointer ptr)
{
	if (ptr) {
		printf("%c", ptr->c);
		preorder(ptr->leftChild);
		preorder(ptr->rightChild);
	}
}

void postorder(treePointer ptr)
{
	if (ptr) {
		postorder(ptr->leftChild);
		postorder(ptr->rightChild);
		printf("%c", ptr->c);
	}
}

void push(treePointer item)
{
	stack[++top] = item;
}

treePointer pop()
{
	return stack[top--];
}

precedence getToken(char *symbol, int *n)
{
	*symbol = expr[(*n)++];
	switch (*symbol) {
	case '(': return lparen;
	case ')': return rparen;
	case '+': return plus;
	case '-': return minus;
	case '/': return divide;
	case '*': return times;
	case '%': return mod;
	case '\0': return eos;
	default: return operand;
	}
}

treePointer eval(void)
{
	precedence token;
	treePointer tp, tp2;

	char symbol;
	int n = 0;
	
	token = getToken(&symbol, &n);

	while (token != eos) {
		if (token == operand) {
			tp = (treePointer)malloc(sizeof(*tp));
			tp->rightChild = NULL;
			tp->leftChild = NULL;
			tp->c = expr[n - 1];
			push(tp);
		}
		else {
			tp2 = (treePointer)malloc(sizeof(*tp2));
			tp2->rightChild = pop();
			tp2->leftChild = pop();

			switch (token) {
			case plus:
				tp2->c = '+';
				push(tp2);
				break;
			case minus:
				tp2->c = '-';
				push(tp2);
				break;
			case times:
				tp2->c = '*';
				push(tp2);
				break;
			case divide:
				tp2->c = '/';
				push(tp2);
				break;
			case mod:
				tp2->c = '%';
				push(tp2);
				break;
			}
		}
		token = getToken(&symbol, &n);
	}
	return stack[0];
}


void main()
{
	FILE* fp = fopen("input.txt", "r");
	char printchar;
	int i=0;

	printf("the length of input string should be less than 80\n");
	printf("input string <postfix expression> : ");
	while (fscanf(fp, "%c", &printchar) != EOF) {
		expr[i] = printchar;
		i++;
	}
	expr[i] = '\0';
	puts(expr);
	printf("\ncreating its binary tree\n\n");
	root = eval();
	
	printf("inorder traversal      : ");
	inorder(root);
	printf("\n");
	printf("preorder traversal     : ");
	preorder(root);
	printf("\n");
	printf("postorder traversal    : ");
	postorder(root);
	printf("\n");
}