#include <stdio.h>
#include <stdlib.h>
#define MAX_ELEMENTS 200

typedef struct {
	int key;
}element;
element heap[MAX_ELEMENTS];
element heap2[MAX_ELEMENTS];
int winner[MAX_ELEMENTS];

void main()
{
	int seed, k, s, i, j, q, temp, count = 0, min, sizeofK;
	int **run;
	printf("seed k(run의 개수) s(run의 크기) : ");
	scanf("%d %d %d", &seed, &k, &s);
	srand(seed);
	sizeofK = k;

	run = (int**)malloc(k*sizeof(*run));
	for (i = 0; i<k; i++) {
		run[i] = (int*)malloc(s*sizeof(**run));
		for (j = 0; j<s; j++) {
			run[i][j] = rand() % 100 + 1;
		}
	}

	for (i = 0; i<k; i++) {
		for (j = 0; j<s; j++) {
			for (q = j; q < s; q++) {
				if (run[i][j] >= run[i][q]) {
					temp = run[i][j];
					run[i][j] = run[i][q];
					run[i][q] = temp;
				}
			}
		}
		heap[i].key = run[i][0];
	}
	for (i = 0; i<k; i++) {
		for (j = 0; j < s; j++) {
			printf("%3d", run[i][j]);
		}
	}


	while (count != 24) {
		for (i = 0; i<k; i++) {
			if (run[i][0] == NULL)
				heap[i].key = 101;
			else
				heap[i].key = run[i][0];
			heap2[i + 8].key = heap[i].key;
		}

		min = heap[0].key;

		for (i = 0; i<k; i++) {
			if (min >= heap[i].key) {
				min = heap[i].key;
				j = i;
			}
		}

		winner[count] = min;

		for (i = 0; i<s; i++) {
			run[j][i] = run[j][i + 1];
			run[j][i + 1] = 101;
			if (run[j][i + 2] > 100 || run[j][i + 2] < 1)
				break;
		}

		sizeofK = k;
		while (sizeofK != 1) {
			for (i = sizeofK; i<sizeofK * 2; i += 2) {
				if (heap2[i].key < heap2[i + 1].key)
					heap2[i / 2].key = heap2[i].key;
				else
					heap2[i / 2].key = heap2[i + 1].key;
			}
			sizeofK /= 2;
		}
		printf("\n\n");
		for (i = 1; i<k * 2; i++)
			printf("%4d", heap2[i]);
		printf("\n");
		count++;
	}

	printf("\nwinner 출력\n");

	for (i = 0; i<count; i++)
		printf("%3d", winner[i]);
	printf("\n");
}