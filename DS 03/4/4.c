#include <stdio.h>
#include <stdlib.h>

int** make2dArray(int rows, int cols)
{
	int i;
	int **x;

	x = (int**)malloc(rows*sizeof(*x));

	for (i = 0; i < rows; i++)
	{
		x[i] = (int*)malloc(cols*sizeof(**x));
	}

	return x;
}

void init2dArray(int **ary, int rows, int cols)
{
	int i, j, num;

	for (i = 0; i < rows; i++)
	{
		num = i;
		for (j = 0; j < cols; j++)
		{
			ary[i][j] = num++;
		}
	}
}

void init2dArray2(int **ary, int rows, int cols)
{
	int i, j, num;

	for (i = 0; i < rows; i++)
	{
		num = i+1;
		for (j = 0; j < cols; j++)
		{
			ary[i][j] = num++;
		}
	}
}

void print2dArray(int **ary, int rows, int cols)
{
	int i, j;

	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < cols; j++)
		{
			printf("%d ", ary[i][j]);
		}
		printf("\n");
	}
}

void free2dArray(int **ary, int rows)
{
	int i;

	for (i = 0; i < rows; i++)
	{
		free(ary[i]);
	}

	free(ary);
}

void add(int **a, int **b, int **c, int rows, int cols)
{
	int i, j;
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < cols; j++)
		{
			c[i][j] = a[i][j] + b[i][j];
		}
	}
}

void main()
{
	int rows, cols;
	int **aryA, **aryB, **aryC;

	printf("Enter row size and column size (ex) 3 4 : ");
	scanf("%d %d", &rows, &cols);

	printf("matrix A\n");
	aryA = make2dArray(rows, cols);

	init2dArray(aryA, rows, cols);
	print2dArray(aryA, rows, cols);
	printf("\n");

	printf("matrix B\n");
	aryB = make2dArray(rows, cols);

	init2dArray2(aryB, rows, cols);
	print2dArray(aryB, rows, cols);
	printf("\n");


	aryC = make2dArray(rows, cols);
	add(aryA, aryB, aryC, rows, cols);
	
	printf("matrix C\n");
	print2dArray(aryC, rows, cols);

	free2dArray(aryA, rows);
	free2dArray(aryB, rows);
	free2dArray(aryC, rows);

	printf("\n2d array - free !!!\n");
}