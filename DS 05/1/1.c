#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_STACK_SIZE 5
#define MAX_NAME_SIZE 20

typedef struct {
	int no;
	char name[MAX_NAME_SIZE];
}element;

element stack[MAX_STACK_SIZE];
int top = -1;

void stackFull()
{
	int i;
	printf("Stack is full, cannot add element\n");
	printf("current stack elements : \n");
	for (i = MAX_STACK_SIZE - 1; i >= 0; i--) {
		printf("%d %s\n", stack[i].no, stack[i].name);
	}
	exit(EXIT_FAILURE);
}

element stackEmpty()
{
	printf("Stack is empty, cannot add element\n");
	exit(EXIT_FAILURE);
}

void push(element item)
{
	if (top >= MAX_STACK_SIZE-1)
		stackFull();
	stack[++top] = item;
	
}

element pop()
{
	if (top == -1)
		return stackEmpty();
	return stack[top--];
}

void main()
{
	char line[100],oper[10];
	element item2;

	printf("<< stack operations where MAX_STACK_SIZE is %d >>\n", MAX_STACK_SIZE);
	printf("**************************************************\n");

	while (1)
	{
		gets(line);

		sscanf(line, "%s %d %s", oper, &item2.no, item2.name);

		if (strcmp(oper, "push") == 0) {
			push(item2);
		}
		else if (strcmp(oper, "pop") == 0) {
			pop();
		}
	}
}
