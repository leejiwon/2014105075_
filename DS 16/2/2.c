#include <stdio.h>
#include <stdlib.h>
#define FALSE 0
#define TRUE 1
#define MAX_VERTICES 20

typedef struct queue *queuePointer;
typedef struct queue {
	int vertex;
	queuePointer link;
}queue;
queuePointer front, rear;
short int visited[MAX_VERTICES];
queuePointer graph[MAX_VERTICES];

void addq(int item)
{
	queuePointer temp;
	temp = (queuePointer)malloc(sizeof(*temp));
	temp->vertex = item;
	temp->link = NULL;
	if (front)
	{
		rear->link = temp;
	}
	else
	{
		front = temp;
	}
	rear = temp;
}

int deleteq()
{
	queuePointer temp = front;
	int item;
	item = temp->vertex;
	front = temp->link;
	free(temp);
	return item;
}

void bfs(int v)
{
	queuePointer w;
	front = rear = NULL;
	printf("%5d", v);
	visited[v] = TRUE;
	addq(v);
	while (front)
	{
		v = deleteq();
		for (w = graph[v]->link; w; w = w->link)
		{
			if (!visited[w->vertex])
			{
				printf("%5d", w->vertex);
				addq(w->vertex);
				visited[w->vertex] = TRUE;
			}
		}
	}
}

void main()
{
	FILE* fp = fopen("input.txt", "r");
	int max_vertex, max_edge, i, j, vertex, edge;
	queuePointer tp[MAX_VERTICES];
	fscanf(fp, "%d %d", &max_vertex, &max_edge);

	for (i = 0; i < max_vertex; i++)
	{
		graph[i] = (queuePointer)calloc(1, sizeof(*tp[i]));
	}

	for (i = 0; i < max_vertex; i++)
	{
		tp[i] = (queuePointer)calloc(1, sizeof(*tp[i]));
	}

	while (fscanf(fp, "%d %d", &vertex, &edge) != EOF)
	{
		queuePointer temp1, temp2;
		temp1 = (queuePointer)calloc(1, sizeof(*temp1));
		temp2 = (queuePointer)calloc(1, sizeof(*temp2));

		if (graph[vertex]->link == NULL)
		{
			temp1->vertex = edge;
			temp1->link = NULL;
			graph[vertex]->link = temp1;
		}
		else
		{
			temp1->vertex = edge;
			temp1->link = graph[vertex]->link;
			graph[vertex]->link = temp1;
		}

		if (graph[edge] == NULL)
		{
			temp2->vertex = vertex;
			temp2->link = NULL;
			graph[edge]->link = temp2;
		}
		else
		{
			temp2->vertex = vertex;
			temp2->link = graph[edge]->link;
			graph[edge]->link = temp2;
		}
	}

	for (i = 0; i < max_vertex; i++)
	{
		tp[i] = graph[i];
	}

	printf("<<<<<<<<<<<< Adjacency List >>>>>>>>>>>>>>\n");
	for (i = 0; i < max_vertex; i++)
	{
		printf("adjList[%d] : ", i);
		for (j = 0; j < max_vertex; j++)
		{
			tp[i] = tp[i]->link;
			if (tp[i] == NULL)
			{
				break;
			}
			printf("%5d", tp[i]->vertex);
		}
		printf("\n");
	}

	printf("\n<<<<<<<<<<<< Depth First Search >>>>>>>>>>>>>>\n");
	for (i = 0; i < max_vertex; i++)
	{
		bfs(i);
		for (j = 0; j < max_vertex; j++)
		{
			visited[j] = 0;
		}
		printf("\n");
	}

}